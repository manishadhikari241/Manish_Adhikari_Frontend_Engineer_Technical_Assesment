import React from 'react'
import { Spin } from 'antd'
import styled from 'styled-components'

const Spinner: React.FC = () => (
  <SpinnerWrapper>
    <Spin />
  </SpinnerWrapper>
)

const SpinnerWrapper = styled.div`
  margin: 20px 0;
  margin-bottom: 20px;
  padding: 30px 50px;
  text-align: center;
  background: rgba(0, 0, 0, 0.05);
  border-radius: 4px;
`

export default Spinner

import React from 'react'
import styled from 'styled-components'

interface Props {
  title?: string
  description?: string
}

const Definition = ({ title, description }: Props) => {
  return (
    <>
      <DefinitionWrapper>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </DefinitionWrapper>
    </>
  )
}

const DefinitionWrapper = styled.div``
const Title = styled.div`
  font-size: 25px;
`
const Description = styled.div``

export default Definition

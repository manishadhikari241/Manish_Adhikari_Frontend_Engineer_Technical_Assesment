import styled from 'styled-components'
import { Container } from 'react-bootstrap'

const HeaderContainer = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  height: 160px;
  padding: 0.5rem 3rem;
  background: #fff;

  @media screen and (max-width: 900px) {
    padding: 2rem;
    height: 120px;
    margin-bottom: 2.5rem;
  }
  border-bottom-left-radius: 2rem;
  border-bottom-right-radius: 2rem;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.8);

  @media screen and (max-width: 500px) {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    box-shadow: none;
  }
`
const Wrapper = styled(Container)`
  top: 0;
  left: 0;
  width: 100%;
  height: 177px;

  @media screen and (max-width: 767px) {
    position: initial;
    height: auto;
    height: 180px;
  }
`
const Intro = styled.div`
  position: relative;
  top: 20px;
  display: flex;
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media screen and (max-width: 900px) {
    display: block;
    top: 0;
  }
`

export { HeaderContainer, Wrapper, Intro }

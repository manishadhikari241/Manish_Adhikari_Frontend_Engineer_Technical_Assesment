import React from 'react'
import Link from 'next/link'
import styled from 'styled-components'

interface props {
  text: string
  link: string
}

const Logo = ({ text, link }: props) => {
  return (
    <TradeMark>
      <Link href={link}>
        <a dangerouslySetInnerHTML={{ __html: text }}></a>
      </Link>
    </TradeMark>
  )
}

const TradeMark = styled.div`
  font-size: 4.5rem;
  font-weight: 900;
  line-height: 3rem;
  letter-spacing: 0.1rem;
  margin-bottom:30px;

  a {
    text-decoration: none;
    color: inherit;
  }
`

export default Logo

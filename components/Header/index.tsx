import React from 'react'
import { Wrapper, HeaderContainer, Intro } from './styles'
import Logo from './Logo'
import Definition from './Definition'

interface Props {
  title?: string
  description?: string
}

const Header = ({ title, description }: Props) => {
  return (
    <Wrapper>
      <HeaderContainer>
        <Intro>
          <Logo text="NECKTIE" link="/"></Logo>
          <Definition title={title} description={description}></Definition>
        </Intro>
      </HeaderContainer>
    </Wrapper>
  )
}

export default Header

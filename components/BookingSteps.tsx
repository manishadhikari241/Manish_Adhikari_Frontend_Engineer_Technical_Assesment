import { Button, Form, message, Steps, theme } from 'antd'
import React, { useState } from 'react'
import { LeftCircleOutlined } from '@ant-design/icons'
import IntroHeader from './Forms/Lib/IntroHeader'
import BookingForm from './BookingForm'
import { useBooking } from '../context/BookingContext'
import { Pages } from '../enums/PageSteps'
import DoctorInfo from './DoctorInfo'
import { bookingApi } from '../services/bookingApi'
import { BookingSubmitRequest } from '../types/Bookings'

const BookingSteps = () => {
  const [form] = Form.useForm()
  const [current, setCurrent] = useState(0)
  const { token } = theme.useToken()
  const { setSteps, selectedDoctor } = useBooking()

  const handleBookingSubmit = async (finalData: BookingSubmitRequest) => {
    try {
      await bookingApi.saveBooking(finalData)
      void message.success('booking Successful')
      setSteps(Pages.INITIAL_PAGE)
    } catch (error) {
      void message.error('Booking Failed')
    }
  }

  const steps = [
    {
      title: 'Doctor',
      content: <DoctorInfo doctor={selectedDoctor}></DoctorInfo>
    },
    {
      title: 'Make an appointment',
      content: (
        <BookingForm
          form={form}
          handleBookingSubmit={handleBookingSubmit}
        ></BookingForm>
      )
    }
  ]
  const next = () => {
    setCurrent(current + 1)
  }

  const prev = () => {
    setCurrent(current - 1)
  }

  const items = steps.map((item) => ({ key: item.title, title: item.title }))

  const contentStyle: React.CSSProperties = {
    lineHeight: '40px',
    textAlign: 'center',
    color: token.colorTextTertiary,
    backgroundColor: token.colorFillAlter,
    borderRadius: token.borderRadiusLG,
    border: `1px dashed ${token.colorBorder}`,
    marginTop: 16
  }

  return (
    <>
      <LeftCircleOutlined
        onClick={() => setSteps(Pages.INITIAL_PAGE)}
        style={{ fontSize: '26px', marginBottom: '10px' }}
      />
      <IntroHeader name={'Make an Appointment'}></IntroHeader>
      <Steps current={current} items={items} />
      <div style={contentStyle}>{steps[current].content}</div>
      <div style={{ marginTop: 24 }}>
        {current < steps.length - 1 && (
          <Button type="primary" onClick={() => next()}>
            Proceed
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button type="primary" onClick={() => form.submit()}>
            Book
          </Button>
        )}
        {current > 0 && (
          <>
            <Button style={{ margin: '0 8px' }} onClick={() => prev()}>
              Previous
            </Button>
            <Button
              style={{ margin: '0 8px' }}
              onClick={() => form.resetFields()}
            >
              Reset
            </Button>
          </>
        )}
      </div>
    </>
  )
}

export default BookingSteps

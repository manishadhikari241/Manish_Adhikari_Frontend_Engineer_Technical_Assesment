import React from 'react'
import styled from 'styled-components'
import { DoctorList } from '../types/DoctorLists'
import { addressBuild } from '../utils/doctorUtils'

interface DoctorInfoProps {
  doctor: DoctorList | null | undefined
}
const DoctorInfo = ({ doctor }: DoctorInfoProps) => {
  return (
    <>
      <ProfileWrapper>
        <Info>{doctor?.name}</Info>
        {addressBuild(doctor?.address)}
      </ProfileWrapper>
    </>
  )
}

const ProfileWrapper = styled.p``
const Info = styled.p``

export default DoctorInfo

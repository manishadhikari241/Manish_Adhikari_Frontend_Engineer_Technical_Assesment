import React from 'react'
import { Avatar, Card, Col, Divider, Row } from 'antd'
import { useBooking } from '../context/BookingContext'
import { Pages } from '../enums/PageSteps'
import ProfileDrawer from './Drawers/ProfileDrawer'
import { DoctorList } from '../types/DoctorLists'
import { addressBuild } from '../utils/doctorUtils'
import IntroHeader from './Forms/Lib/IntroHeader'
import styled from 'styled-components'
import { UserOutlined } from '@ant-design/icons'

const { Meta } = Card

interface Props {
  doctorList: DoctorList[]
}

const ProfileCard = ({ doctorList }: Props) => {
  const { setSteps, selectDoctor } = useBooking()

  const handleBookNow = (doctor: DoctorList) => {
    setSteps(Pages.BOOKING_PAGE)
    selectDoctor(doctor)
  }

  return (
    <Row justify="start" gutter={{ sm: 16, md: 16 }}>
      <Divider orientation="left" plain>
        <IntroHeader name="Top Doctors"></IntroHeader>
      </Divider>
      <>
        {doctorList.map((doctor, index) => (
          <Col
            xs={{ span: 24 }}
            sm={{ span: 16 }}
            lg={{ span: 8 }}
            md={{ span: 12 }}
            xl={{ span: 8 }}
            className="gutter-row"
            span={8}
            key={index}
          >
            <CardWrapper>
              <Card
                style={{ width: '100%' }}
                actions={[
                  <div
                    key={Pages.BOOKING_PAGE}
                    onClick={() => handleBookNow(doctor)}
                  >
                    Book Now{' '}
                  </div>,
                  <div key={Pages.PROFILE_DRAWER}>
                    <ProfileDrawer doctorId={doctor.id} drawerPlacement="right"></ProfileDrawer>
                  </div>
                ]}
              >
                <Meta
                  avatar={<Avatar icon={<UserOutlined />}></Avatar>}
                  title={doctor?.name}
                  description={
                    <>
                      <div>{doctor?.description}</div>
                      <div>{addressBuild(doctor.address)}</div>
                    </>
                  }
                />
              </Card>
            </CardWrapper>
          </Col>
        ))}
      </>
    </Row>
  )
}

const CardWrapper = styled.div`
  padding: 8px 0px;
`

export default ProfileCard

import { PoweroffOutlined } from '@ant-design/icons'
import { Drawer, DrawerProps, FloatButton } from 'antd'
import React, { useState } from 'react'
import { useQuery } from 'react-query'
import { useAuthorization } from '../../context/AuthorizationContext'
import { bookingApi } from '../../services/bookingApi'
import { getDrawerWidth } from '../../utils/commonUtils'
import IntroHeader from '../Forms/Lib/IntroHeader'
import Spinner from '../Library/Spinner'
import { BookingTable } from '../TableColumns/BookingTable'

interface BookingsDrawerProps {
  drawerPlacement: DrawerProps['placement']
}

const BookingsDrawer = ({ drawerPlacement }: BookingsDrawerProps) => {
  const { forbidAuthorization } = useAuthorization()
  const [open, setOpen] = useState(false)

  const { isLoading, data: bookings } = useQuery(
    ['get_all_bookings', open],
    async () => await bookingApi.getBookings(),
    {
      enabled: open
    }
  )

  const showDrawer = () => {
    setOpen(true)
  }
  const onClose = () => {
    setOpen(false)
  }
  return (
    <div>
      <FloatButton
        shape="square"
        type="primary"
        style={{ right: 24, width: '70px' }}
        description="Bookings"
        onClick={showDrawer}
      />
      <FloatButton
        shape="square"
        type="primary"
        style={{ right: 24, bottom: 100 }}
        icon={<PoweroffOutlined />}
        onClick={forbidAuthorization}
      />
      <Drawer
        width={getDrawerWidth()}
        title=""
        placement={drawerPlacement}
        onClose={onClose}
        open={open}
      >
        {isLoading
          ? (
          <Spinner></Spinner>
            )
          : (
          <>
            <IntroHeader name="All your Bookings"></IntroHeader>
            <BookingTable bookings={bookings}></BookingTable>
          </>
            )}
      </Drawer>
    </div>
  )
}

export default BookingsDrawer

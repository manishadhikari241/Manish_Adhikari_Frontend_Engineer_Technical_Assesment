import React, { useState } from 'react'
import { Drawer, Table } from 'antd'
import type { DrawerProps } from 'antd/es/drawer'
import styled from 'styled-components'
import IntroHeader from '../Forms/Lib/IntroHeader'
import { useQuery } from 'react-query'
import { doctorApi } from '../../services/doctorApi'
import Spinner from '../Library/Spinner'
import DoctorInfo from '../DoctorInfo'
import { DoctorAvailabilityColumnn } from '../TableColumns/DoctorAvailability'
import { getDrawerWidth } from '../../utils/commonUtils'

interface ProfileDrawerProps {
  drawerPlacement: DrawerProps['placement']
  doctorId: string
}

const ProfileDrawer = ({ drawerPlacement, doctorId }: ProfileDrawerProps) => {
  const [open, setOpen] = useState(false)
  const { isLoading, data: doctorProfile } = useQuery(
    ['get_doctor_profile', doctorId, open],
    async () => await doctorApi.getDoctorProfile(doctorId),
    {
      enabled: open
    }
  )
  const showDrawer = () => {
    setOpen(true)
  }
  const onClose = () => {
    setOpen(false)
  }
  return (
    <>
      <SpanText onClick={showDrawer}>Profile</SpanText>
      <Drawer
        width={getDrawerWidth()}
        title={doctorProfile?.name}
        placement={drawerPlacement}
        onClose={onClose}
        open={open}
      >
        {isLoading
          ? (
          <Spinner></Spinner>
            )
          : (
          <>
            <IntroHeader name="Information"></IntroHeader>
            <DoctorInfo doctor={doctorProfile}></DoctorInfo>
            <IntroHeader name="Available Hours"></IntroHeader>

            <Table
              dataSource={doctorProfile?.opening_hours}
              columns={DoctorAvailabilityColumnn}
            />
          </>
            )}
      </Drawer>
    </>
  )
}

const SpanText = styled.div``

export default ProfileDrawer

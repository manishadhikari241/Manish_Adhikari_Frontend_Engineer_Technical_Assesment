
import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons'

export const DoctorAvailabilityColumnn = [
  {
    title: 'Day',
    dataIndex: 'day',
    key: 'day'
  },
  {
    title: 'Start',
    dataIndex: 'start',
    key: 'start'
  },
  {
    title: 'End',
    dataIndex: 'end',
    key: 'end'
  },
  {
    title: 'Availability',
    dataIndex: 'isClosed',
    key: 'isClosed',
    render: (isClosed: boolean) => {
      return isClosed
        ? (
          <CloseCircleTwoTone twoToneColor="#52c41a" />
          )
        : (
          <CheckCircleTwoTone twoToneColor="#52c41a" />
          )
    }
  }
]

import { CheckCircleTwoTone, CloseCircleTwoTone } from '@ant-design/icons'
import { Button, message, Table } from 'antd'
import { useState } from 'react'
import { BookingStatus } from '../../enums/BookingStatus'
import { bookingApi } from '../../services/bookingApi'
import { Bookings } from '../../types/Bookings'
import { getTimeFromDecimal } from '../../utils/dateTimeUtils'

interface BookingTableProps {
  bookings: Bookings[] | undefined
}

export const BookingTable = ({ bookings }: BookingTableProps) => {
  const [bookingTableData, setBookingTableData] = useState<
  Bookings[] | undefined
  >(bookings)

  const updateBooking = async (id: string, status: string) => {
    const changeStatus =
      status === BookingStatus.CONFIRMED
        ? BookingStatus.CANCELLED
        : BookingStatus.CONFIRMED
    try {
      const updatedData = await bookingApi.updateBookingStatus(id, changeStatus)
      setBookingTableData(
        bookingTableData?.map((data) =>
          data?.id === updatedData?.id ? { ...data, ...updatedData } : data
        )
      )
      void message.success('Your booking has been updated')
    } catch (error) {
      void message.error('Failed to update booking')
    }
  }
  const handleBookingAction = (id: string, status: string) => {
    void updateBooking(id, status)
  }
  const BookingsColumn = [
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date'
    },
    {
      title: 'Doctor',
      dataIndex: 'doctorId',
      key: 'doctorId'
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: 'Start',
      dataIndex: 'start',
      key: 'start',
      render: (time: string) => {
        return getTimeFromDecimal(parseFloat(time))
      }
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (status: string) => {
        switch (status) {
          case BookingStatus.CANCELLED:
            return <CloseCircleTwoTone twoToneColor="#52c41a" />
          default:
            return <CheckCircleTwoTone twoToneColor="#52c41a" />
        }
      }
    },
    {
      title: 'Action',
      dataIndex: 'id',
      key: 'id',
      render: (id: string, data: Bookings) => {
        return (
          <Button onClick={() => handleBookingAction(id, data.status)}>
            {data.status === BookingStatus.CANCELLED ? 'Confirm' : 'Cancel'}
          </Button>
        )
      }
    }
  ]

  return <Table dataSource={bookingTableData} columns={BookingsColumn} />
}

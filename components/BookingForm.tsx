import { DatePicker, Form, Input, Radio } from 'antd'
import { RangePickerProps } from 'antd/es/date-picker'
import type { FormInstance } from 'antd/es/form'
import dayjs from 'dayjs'
import { useEffect, useState } from 'react'
import { useBooking } from '../context/BookingContext'
import { BookingFormItem, BookingSubmitRequest } from '../types/Bookings'
import {
  generateTimeSlots,
  getDaysOfWeek,
  getTimeFromDecimal
} from '../utils/dateTimeUtils'
import { isNil } from 'ramda'

interface Props {
  handleBookingSubmit: (finalData: BookingSubmitRequest) => Promise<void>
  form: FormInstance
}

const BookingForm = ({ handleBookingSubmit, form }: Props) => {
  const { selectedDoctor } = useBooking()
  const [timeSlots, setTimeSlots] = useState<number[]>([])
  const date = Form.useWatch('date', form)

  useEffect(() => {
    if (isNil(date)) setTimeSlots([])
  }, [date])

  useEffect(() => {
    if (timeSlots.length === 0) {
      form.setFieldValue('start', null)
      form.setFieldValue('date', null)
    }
  }, [timeSlots])

  const layout = {
    labelCol: { span: 16 },
    wrapperCol: { span: 16 }
  }

  const complete = (values: BookingSubmitRequest) => {
    const date = dayjs(values?.date).format('YYYY-MM-DD')
    const doctorId = selectedDoctor?.id ?? ''
    const finalData: BookingSubmitRequest = { ...values, date, doctorId }
    void handleBookingSubmit(finalData)
  }

  const onError = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  const onFormItemChange = (changedValues: BookingFormItem) => {
    if (!isNil(changedValues?.date)) {
      const day = getDaysOfWeek(changedValues?.date)
      const selectedDay = selectedDoctor?.opening_hours?.find(
        (opening) => opening.day === day && !opening.isClosed
      )
      // Hardcoded slot range to 1, every 1 hour interval as per requirement
      if (!isNil(selectedDay)) {
        setTimeSlots(
          generateTimeSlots(
            parseFloat(selectedDay.start),
            parseFloat(selectedDay.end),
            1
          )
        )
      } else {
        setTimeSlots([])
      }
    }
  }

  const disabledDate: RangePickerProps['disabledDate'] = (current) => {
    // Can not select days before today and today
    return current < dayjs().endOf('day')
  }

  return (
    <>
      <Form
        form={form}
        {...layout}
        layout="vertical"
        style={{ width: '100%' }}
        initialValues={{}}
        onFinish={ complete}
        onFinishFailed={onError}
        autoComplete="off"
        onValuesChange={(changedValues) => onFormItemChange(changedValues)}
      >
        <Form.Item
          label="Contact Information"
          name="name"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input placeholder="Name" />
        </Form.Item>
        <Form.Item
          name="date"
          label="Choose the day you are available for appointment"
          rules={[{ required: true, message: 'Please pick an item!' }]}
        >
          <DatePicker
            disabledDate={disabledDate}
            format="YYYY-MM-DD"
            style={{ width: '100%' }}
          />
        </Form.Item>
        <Form.Item
          name="start"
          label="Days of week you are available for the appointment"
          rules={[{ required: true, message: 'Please pick an item!' }]}
        >
          <Radio.Group>
            {timeSlots.map((slots, index) => (
              <Radio.Button key={index} value={slots}>
                {getTimeFromDecimal(slots)}
              </Radio.Button>
            ))}
          </Radio.Group>
        </Form.Item>
      </Form>
    </>
  )
}

export default BookingForm

import React from 'react'
import styled from 'styled-components'

interface Props {
  name: string
}

const IntroHeader = ({ name }: Props) => {
  return <Title>{name}</Title>
}

const Title = styled.div`
  font-size: 25px;
  margin-bottom: 20px;
`

export default IntroHeader

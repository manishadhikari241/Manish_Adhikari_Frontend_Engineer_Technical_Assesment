import React from 'react'
import { Input } from 'antd'

interface Props {
  placeholder: string
}

const TextInput = ({ placeholder }: Props) => {
  return (
    <Input placeholder={placeholder} />
  )
}

export default TextInput

export interface Address {
  district: string
  line_1: string
  line_2: string
}
interface OpeningHours {
  day: string
  end: string
  isClosed: boolean
  start: string
}
export interface DoctorList {
  id: string
  address: Address
  description: string
  name: string
  opening_hours: OpeningHours[]
}

export interface Bookings {
  date: string
  doctorId: string
  id: string
  name: string
  start: number
  status: string
}

export interface BookingFormItem {
  date?: string
  start?: number
  name?: string
}

export interface BookingSubmitRequest {
  name: string
  start: number
  doctorId: string
  date: string
}

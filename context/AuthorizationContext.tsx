import { deleteCookie, getCookie, setCookie } from 'cookies-next'
import { useRouter } from 'next/router'
import { isNil } from 'ramda'
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect
} from 'react'
import { CustomHeader } from '../enums/CommonHeader'

interface AuthorizationContextProviderProps {
  children: ReactNode
}

interface AuthorizationKey {
  authorize: (key: string) => void
  forbidAuthorization: () => void
}

const AuthorizationContext = createContext<AuthorizationKey>({
  authorize: () => {},
  forbidAuthorization: () => {}

})

export const useAuthorization = () => {
  return useContext(AuthorizationContext)
}

export function AuthorizationProvider({
  children
}: AuthorizationContextProviderProps) {
  const router = useRouter()

  const isAuthorized = useCallback(() => {
    const savedApiKey = getCookie(CustomHeader.AuthorizationHeader)
    return !isNil(savedApiKey)
  }, [])

  useEffect(() => {
    if (!isAuthorized()) void router.push('/authorize')
  }, [])

  const authorize = (key: string) => {
    setCookie(CustomHeader.AuthorizationHeader, key)
  }

  const forbidAuthorization = () => {
    deleteCookie(CustomHeader.AuthorizationHeader)
    void router.push('/authorize')
  }

  return (
    <AuthorizationContext.Provider value={{ authorize, forbidAuthorization }}>
      {children}
    </AuthorizationContext.Provider>
  )
}

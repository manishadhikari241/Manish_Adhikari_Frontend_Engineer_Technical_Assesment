import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useState
} from 'react'
import { Pages } from '../enums/PageSteps'
import { DoctorList } from '../types/DoctorLists'

interface BookingContextType {
  pageSteps: number
  setSteps: (steps: number) => void
  selectedDoctor: DoctorList | null
  selectDoctor: (doctor: DoctorList) => void
}

const BookingContext = createContext<BookingContextType>({
  pageSteps: Pages.INITIAL_PAGE,
  setSteps: () => {},
  selectedDoctor: null,
  selectDoctor: () => {}
})

interface BookingContextProviderProps {
  children: ReactNode
}

export const useBooking = () => {
  return useContext(BookingContext)
}

export const BookingProvider = ({ children }: BookingContextProviderProps) => {
  const [pageSteps, setPageSteps] = useState<number>(Pages.INITIAL_PAGE)
  const [selectedDoctor, setSelectedDoctor] = useState<DoctorList | null>(null)

  const setSteps = useCallback((steps: number) => {
    setPageSteps(steps)
  }, [])

  const selectDoctor = useCallback((doctor: DoctorList) => {
    setSelectedDoctor(doctor)
  }, [])

  return (
    <BookingContext.Provider
      value={{ setSteps, pageSteps, selectDoctor, selectedDoctor }}
    >
      {children}
    </BookingContext.Provider>
  )
}

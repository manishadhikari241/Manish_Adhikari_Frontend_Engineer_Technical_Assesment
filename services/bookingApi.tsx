import { Bookings, BookingSubmitRequest } from '../types/Bookings'
import { fetchApi } from './interceptors'

const getBookings = async () => {
  try {
    const getBookingsRsp = await fetchApi.get<Bookings[]>('/booking')
    return getBookingsRsp.data
  } catch (error) {
    console.error(error)
  }
}
const saveBooking = async (bookingRequestData: BookingSubmitRequest) => {
  try {
    const getBookingsRsp = await fetchApi.post<Bookings[]>(
      '/booking',
      bookingRequestData
    )
    return getBookingsRsp.data
  } catch (error) {
    throw new Error('Booking Failed')
  }
}

const updateBookingStatus = async (id: string, status: string) => {
  try {
    const getBookingsRsp = await fetchApi.patch<Bookings>(`/booking/${id}`, {
      status
    })
    return getBookingsRsp.data
  } catch (error) {
    throw new Error('Booking Failed to update')
  }
}

export const bookingApi = {
  getBookings,
  saveBooking,
  updateBookingStatus
}

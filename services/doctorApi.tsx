import { CustomHeader } from '../enums/CommonHeader'
import { DoctorList } from '../types/DoctorLists'
import { fetchApi } from './interceptors'

const getDoctorLists = async (xApiKey: string | undefined) => {
  let response: DoctorList[] = []
  try {
    const doctorListRequest = await fetchApi.get<DoctorList[]>('/doctor', {
      // For SSR we need to explicitly set the xapikey header from the request, for CSR its globally set
      headers: {
        [CustomHeader.AuthorizationHeader]: xApiKey
      }
    })
    response = doctorListRequest.data
  } catch (error) {
    console.error(error)
  }
  return response
}

const getDoctorProfile = async (id: string) => {
  try {
    const getdoctorProfileRsp = await fetchApi.get<DoctorList>(`/doctor/${id}`)
    return getdoctorProfileRsp.data
  } catch (error) {
    console.error(error)
  }
}

export const doctorApi = {
  getDoctorLists,
  getDoctorProfile
}

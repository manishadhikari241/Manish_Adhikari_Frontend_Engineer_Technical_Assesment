import axios from 'axios'
import { getCookie } from 'cookies-next'
import { isNil } from 'ramda'
import { CustomHeader } from '../enums/CommonHeader'

const baseUrl: string | undefined = process.env.NEXT_PUBLIC_API_URL

export const fetchApi = axios.create({
  baseURL: baseUrl
})
fetchApi.interceptors.request.use(
  (config) => {
    const xApiKey = getCookie(CustomHeader.AuthorizationHeader)?.toString()
    if (isNil(config.headers[CustomHeader.AuthorizationHeader])) {
      config.headers[CustomHeader.AuthorizationHeader] = xApiKey
    }
    return config
  },
  async (error) => {
    return await Promise.reject(error)
  }
)

## Getting Started

Production URL https://doctor-booking-mu.vercel.app/

The application is built using NextJs(a react framework) using Typescript with complete type definition.

Why Next Js ?
Next js is a React framework that offers Pre-rendering(SSG/SSR/Universal) which will boost performance and enhance the speed of loading page and offers flexibility of the application development by its boilerplate templates.

Why Typescript ?
TypeScript is better than JavaScript in terms of language features, reference validation, project scalability, collaboration within and between teams, developer experience, and code maintainability. Static typing allows you to catch errors earlier in the debugging/development process.

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## Code Quality
For the code quality and maintainability, code linting is performed using strict type checking.
Linting will be done in both local machine before git commit and it will also be checked on gitlab ci pipeline.

Additionals:
Pre commit hooks has been implemented which will do the linting check and commit message validation before pushing into the repository. 
Use proper commit message before making a commit otherwise it will fail. Refer to the .husky folder for the convention to be followed. Eg (feat: my new project, fix: bug fixed)

## Git flow and Deployment
Checkout from master branch(source of truth) to feature branch. Merge feature branch back to master.
The project is deployed to vercel using gitlab ci pipeline.

Steps:
1. Make a commit following the commit message convention(refer to./husky folder)
2. Wait for the pre-commit hook to run. Use npm run lint-fix for quick fixing of tsx,jsx.js,ts files.
3. Send merge request to master branch once the ci pipeline is passed.
4. Check for deployment preview and merge to master if everything is good.  
5. Deployed to Production !! 

## Project Notes

Time in float value eg (16.50 means 16:50 or 16:30). The logic has to be mentioned clearly if the float value is the actual minutes. Current implementation (16.50 means 16:30)
/booking/{id}=> endpoint is not used as the model from /booking is same.

## Areas of improvement
{
    "day": "WED",
     "end": "19.00",
    "isClosed": false,
    "start": "9.00"
 },

The Api schema for the start and end which is provided in decimal can be improved by providing the actual time instead of a decimal value which is rather confusing. Or could be mentioned clearly the of its usage.

Unit Testing could be implemented.

export enum Pages {
  INITIAL_PAGE = 0,
  BOOKING_PAGE = 1,
  PROFILE_DRAWER = 3,
  YOUR_BOOKINGS = 4
}

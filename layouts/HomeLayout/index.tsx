import React from 'react'
import { Container } from 'react-bootstrap'
import Header from '../../components/Header'
import styled from 'styled-components'

interface props {
  children: React.ReactNode
}

const HomeLayout: React.FC<props> = ({ children }) => {
  return (
    <div>
      <Header title="Doctor Booking Application" />
      <Container>
        <MainWrapper>{children}</MainWrapper>
      </Container>
    </div>
  )
}

const MainWrapper = styled.div`
  width: 90%;
  margin: auto;
  margin-bottom :30px;
`

export default HomeLayout

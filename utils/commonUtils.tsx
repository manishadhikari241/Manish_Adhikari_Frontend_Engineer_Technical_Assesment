import { isNil } from 'ramda'

export const getWindowWidth = () => {
  if (typeof window !== 'undefined') {
    return window?.innerWidth
  }
}

export const getDrawerWidth = (): number | undefined => {
  const windowWidth = getWindowWidth()
  if (isNil(windowWidth)) return undefined
  return windowWidth > 900 ? 800 : windowWidth - 10
}

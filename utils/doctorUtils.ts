import { isNil } from 'ramda'
import { Address } from '../types/DoctorLists'

export const addressBuild = (address: Address | undefined) => {
  if (isNil(address)) return ''
  const fullAddress: string = ''
  return fullAddress.concat(address?.line_1, ', ', address?.line_2, ', ', address.district)
}

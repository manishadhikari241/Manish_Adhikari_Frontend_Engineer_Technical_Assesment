import dayjs from 'dayjs'

const daysMapping = [
  'SUN',
  'MON',
  'TUE',
  'WED',
  'THU',
  'FRI',
  'SAT'
]

export const getDaysOfWeek = (date: string | undefined) => {
  return daysMapping[dayjs(date).day()]
}

export const generateTimeSlots = (start: number, end: number, frequency: number): number[] => {
  const slots: number[] = []
  let i
  for (i = start; i <= end; i = i + frequency) {
    if (i < (end)) { slots.push(i) }
  }
  return slots
}

export const getTimeFromDecimal = (number: number) => {
  const sign = (number >= 0) ? 1 : -1

  // Set positive value of number of sign negative
  number = number * sign

  // Separate the int from the decimal part
  const hour = Math.floor(number)
  let decpart = number - hour

  const min = 1 / 60
  // Round to nearest minute
  decpart = min * Math.round(decpart / min)
  let minute = Math.floor(decpart * 60).toString() + ''

  // Add padding if need
  if (minute.length < 2) {
    minute = '0' + minute
  }

  // Add Sign in final result
  const finalSign = sign === 1 ? '' : '-'

  // Concate hours and minutes
  return finalSign + hour.toString() + ':' + minute.toString()
}

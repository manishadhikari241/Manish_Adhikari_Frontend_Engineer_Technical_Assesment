import { getCookie } from 'cookies-next'
import { GetServerSideProps } from 'next'
import React, { Suspense } from 'react'
import BookingSteps from '../components/BookingSteps'
import BookingsDrawer from '../components/Drawers/BookingsDrawer'
import ProfileCard from '../components/ProfileCard'
import { useBooking } from '../context/BookingContext'
import { CustomHeader } from '../enums/CommonHeader'
import { Pages } from '../enums/PageSteps'
import HomeLayout from '../layouts/HomeLayout'
import { doctorApi } from '../services/doctorApi'
import { DoctorList } from '../types/DoctorLists'

interface HomePageProps {
  doctorList: DoctorList[]
}

const index = ({ doctorList }: HomePageProps) => {
  const { pageSteps } = useBooking()

  const handlePages = (): JSX.Element => {
    switch (pageSteps) {
      case Pages.BOOKING_PAGE:
        return <BookingSteps></BookingSteps>
      default:
        return (
          <>
            <ProfileCard doctorList={doctorList}></ProfileCard>
            <BookingsDrawer drawerPlacement="left"></BookingsDrawer>
          </>
        )
    }
  }
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <HomeLayout>{handlePages()}</HomeLayout>
    </Suspense>
  )
}

export const getServerSideProps: GetServerSideProps<{
  doctorList: DoctorList[]
}> = async ({ req, res }) => {
  const xApiKey = getCookie(CustomHeader.AuthorizationHeader, {
    req,
    res
  })?.toString()

  const doctorList = await doctorApi.getDoctorLists(xApiKey)

  return {
    props: {
      doctorList
    }
  }
}

export default index

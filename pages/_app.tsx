import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { AuthorizationProvider } from '../context/AuthorizationContext'
import { BookingProvider } from '../context/BookingContext'
import { QueryClientProvider, QueryClient } from 'react-query'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      cacheTime: 0,
      retry: false
    }
  }
})

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={queryClient}>
      <AuthorizationProvider>
        <BookingProvider>
          <Component {...pageProps} />
        </BookingProvider>
      </AuthorizationProvider>
    </QueryClientProvider>
  )
}

export default MyApp

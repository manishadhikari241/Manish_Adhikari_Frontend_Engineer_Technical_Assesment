import { Button, Form, Input } from 'antd'
import { useRouter } from 'next/router'
import React from 'react'
import { Container } from 'react-bootstrap'
import { useAuthorization } from '../context/AuthorizationContext'
import HomeLayout from '../layouts/HomeLayout'

interface FormValues {
  authorization_key: string
}

const authorize = () => {
  const router = useRouter()
  const { authorize } = useAuthorization()

  const onFinish = (formValues: FormValues): void => {
    authorize(formValues.authorization_key)
    void router.push('/')
  }

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo)
  }

  return (
    <HomeLayout>
      <Container>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Login with Authorization key"
            name="authorization_key"
            rules={[{ required: true, message: 'Please authorize' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Container>
    </HomeLayout>
  )
}

export default authorize
